(import ./. {}).shellFor {
  tools = {
    cabal = "latest";
    haskell-language-server = "latest";
  };
}
