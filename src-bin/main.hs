{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

import Text.ICalendar.Parser
import Text.ICalendar.Printer
import Text.ICalendar.Types
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Witherable
import Data.ByteString.Lazy ()
import qualified Data.ByteString.Lazy as LBS
import Control.Monad
import Data.Default
import Network.HTTP.Types.Status (statusCode, status200, status404)
import Network.HTTP.Types.Header
import Control.Lens
import Control.Lens.TH
import Network.Wai.Handler.Warp
import Network.Wai
import System.Environment
import Control.Exception
import Data.Text (Text)
import qualified Data.Text as T
import Data.Aeson
import Data.Aeson.TH
import qualified Data.Set as Set
import Text.Read

makeLensesWith (lensRules & lensField .~ mappingNamer (\n -> ["_" <> n])) ''VCalendar

data Config = Config
  { _config_upstreamUri :: String
  , _config_path :: Text
  }

deriveJSON (defaultOptions { fieldLabelModifier = drop 1 . dropWhile (/= '_') . drop 1 }) ''Config

main :: IO ()
main = do
  (portRaw, cfgFilePath) <- getArgs >>= \case
    [portRaw, cfgFilePath] -> pure (portRaw, cfgFilePath)
    _ -> fail "Expected exactly two arguments: the port and the configuration file path"
  cfgRaw <- try @SomeException (LBS.readFile cfgFilePath) >>= \case
    Left e -> fail $ "Error reading config file: " <> show e
    Right cfgRaw -> pure cfgRaw
  port <- case readMaybe portRaw of
    Nothing -> fail "Error parsing port number"
    Just port -> pure port
  cfg <- case eitherDecode' cfgRaw of
    Left e -> fail $ "Error decoding config file: " <> e
    Right cfg -> pure cfg
  mgr <- newManager tlsManagerSettings
  upstreamReq <- case parseRequest $ _config_upstreamUri cfg of
    Left e -> fail $ "Error interpreting upstreamUri from config: " <> show e
    Right upstreamReq -> pure upstreamReq
  run port $ \req respond -> case pathInfo req == T.splitOn "/" (_config_path cfg) of
    False -> respond $ responseLBS status404 [] ""
    True -> do
      rsp <- httpLbs upstreamReq mgr
      when (statusCode (Network.HTTP.Client.responseStatus rsp) /= 200) $ fail "Error retrieving calendar"
      filtered <- filterIcs $ responseBody rsp
      respond $ responseLBS status200 [(hContentType, "text/calendar")] filtered

filterIcs :: MonadFail m => LBS.ByteString -> m LBS.ByteString
filterIcs ics = case parseICalendar def "upstream.ics" ics of
  Left e -> fail $ "Error parsing calendar" --Don't show the error, since it could contain private info
  Right (items, []) -> pure $ mconcat $ fmap (printICalendar def) $ filterCalendars items

--TODO: Get rid of Todos, Journals, and any other info not known to be safe
--TODO: Should we convert events into FreeBusys?
filterCalendars :: [VCalendar] -> [VCalendar]
filterCalendars = fmap $ \c -> VCalendar
  { vcProdId = vcProdId c
  , vcVersion = vcVersion c
  , vcScale = vcScale c
  , vcMethod = vcMethod c --TODO: This should probably be checked to ensure it's the right method
  , vcOther = Set.fromList
    [ OtherProperty
      { otherName = "X-PUBLISHED-TTL"
      , otherValue = "DURATION:PT1M"
      , otherParams = def
      }
    , OtherProperty
      { otherName = "REFRESH-INTERVAL"
      , otherValue = "DURATION:PT1M"
      , otherParams = def
      }
    ]
  , vcTimeZones = vcTimeZones c
  , vcEvents = mapMaybe filterEvent $ vcEvents c
  , vcTodos = mempty
  , vcJournals = mempty
  , vcFreeBusys = mempty
  , vcOtherComps = mempty
  }

filterEvent :: VEvent -> Maybe VEvent
filterEvent e = case veTransp e of
  Transparent _ -> Nothing
  Opaque _ -> Just $ VEvent
    -- NOTE: We list all things to keep explicitly here (rather than updating
    -- the existing record `e`), so that if new items were added to the record
    -- in future, they would not automatically be kept
    -- Things to keep:
    { veDTStamp = veDTStamp e
    , veUID = veUID e
    , veClass = veClass e
    , veDTStart = veDTStart e
    , veTransp = veTransp e
    , veRecurId = veRecurId e
    , veRRule = veRRule e
    , veDTEndDuration = veDTEndDuration e
    , veExDate = veExDate e
    , veRDate = veRDate e
    -- Placeholder
    , veSummary = Just $ Summary
      { summaryValue = "Busy"
      , summaryAltRep = Nothing
      , summaryLanguage = Nothing
      , summaryOther = def
      }
    -- Things to erase:
    , veCreated = Nothing
    , veDescription = Nothing
    , veGeo = Nothing
    , veLastMod = Nothing
    , veLocation = Nothing
    , veOrganizer = Nothing
    , vePriority = def
    , veSeq = def
    , veStatus = Nothing
    , veUrl = Nothing
    , veAttach = mempty
    , veAttendee = mempty
    , veCategories = mempty
    , veComment = mempty
    , veContact = mempty
    , veRStatus = mempty
    , veRelated = mempty
    , veResources = mempty
    , veAlarms = mempty
    , veOther = mempty
    }
