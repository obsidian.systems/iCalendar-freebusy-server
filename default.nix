{ haskellNix ? import ./dep/haskell.nix {}
}:

let inherit (import ./dep/nix-thunk {}) thunkSource;
    pkgs = haskellNix.pkgs-unstable;
    packageDef = { name, version, src, signatures ? [ ], type ? "Targets", expires ? null }: {
      inherit signatures;
      signed = {
        "_type" = type;
        inherit expires;
        targets = {
          "<repo>/package/${name}-${version}.tar.gz" = {
            hashes = {
              sha256 = builtins.hashFile "sha256" "${src}/${name}.cabal";
            };
            length = 1;
          };
        };
        version = 0;
      };
    };

    genBuildCommands = defs: map (a: let
      name = a.name;
      version = a.version;
      json = builtins.toFile "${a.name}.json" (builtins.toJSON (packageDef { inherit (a) name version src; }));
    in ''
      mkdir -p $packagedef/${name}/${version}
      cp ${json} $packagedef/${name}/${version}/package.json
      cp ${a.src}/*.cabal $packagedef/${name}/${version}
    '') defs;

    writePackageDefs = defs: pkgs.runCommand "index.tar.gz" {
      outputs = [ "packagedef" "out" ];
    } ''
      set -eux
      ${builtins.concatStringsSep "\n" defs}
      mkdir -p $packagedef
      cd $packagedef
      tar --sort=name --owner=root:0 --group=root:0 --mtime='UTC 2009-01-01' -hczvf $out */*/*
    '';

    genHackageForNix = hackagetar: pkgs.runCommand "hackage-for-nix" { } ''
      export LANG=C.UTF-8
      cp ${hackagetar} 01-index.tar.gz
      ${pkgs.gzip}/bin/gunzip 01-index.tar.gz
      ${pkgs.haskell-nix.nix-tools}/bin/hackage-to-nix $out 01-index.tar "https://hackagefornix/"
    '';

    defs = [
      { name = "iCalendar"; version = "0.4.0.6"; src = thunkSource ./dep/iCalendar; }
    ];
    tarball = (writePackageDefs (genBuildCommands defs)).out;
in

pkgs.haskell-nix.cabalProject {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "iCalendar-freebusy-server";
    src = ./.;
  };
  compiler-nix-name = "ghc982";
  modules = map (a: { packages.${a.name}.src = a.src; }) defs;
  extra-hackage-tarballs = {
    overlay = tarball;
  };
  extra-hackages = [
    (import (genHackageForNix tarball))
  ];
}
