# iCalendar-freebusy-server

iCalendar-freebusy-server transforms .ics files by redacting every event so that only free/busy information remains.  This allows a private calendar to be imported into another calendar application without revealing private information.

I created this because I was not able to find a way to produce a free/busy ICS file from a Google calendar.  It can share free/busy with other Google users, but it doesn't seem to be able to do that with external services.

To use this, you will need to run it as a service on a trusted computer.  As the argument to the `freebusy-server` executable, provide the port number and then path to a file like this:

```json
{
  "upstreamUri": "https://calendar.google.com/calendar/ical/someone%40somewhere.com/private-0123456789abcdef0123456789abcdef/basic.ics",
  "path": "freebusy.ics"
}
```

The upstreamUri should be a location where this program can retrieve the original calendar.  It will serve the redacted calendar at the given path; by choosing a path that is difficult to guess, you can keep the free/busy information somewhat secure.

Note that the ICS files this produces will not be scrubbed of *all* identifying information.  The UIDs of events, time zones, etc. will pass through.  Merge requests that improve this situation are welcome!
